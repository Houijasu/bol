local Script_Version = "0.01"
local Script_Name = "Ashe"
local Script_Author = "Houijasu"

if myHero.charName ~= Script_Name then return end

-- | Auto Script Update Part by Honda7 ~Thanks | Starts
local AUTOUPDATE= true
local UPDATE_SCRIPT_NAME = Script_Name
local UPDATE_NAME = Script_Name
local UPDATE_HOST = "raw.github.com"
local UPDATE_PATH = "/"..Script_Author.."/BoL/master/"..Script_Name..".lua".."?rand="..math.random(1,10000)
local UPDATE_FILE_PATH = SCRIPT_PATH..GetCurrentEnv().FILE_NAME
local UPDATE_URL = "https://"..UPDATE_HOST..UPDATE_PATH
function AutoupdaterMsg(msg) print("<font color=\"#6699ff\"><b>"..UPDATE_NAME..":</b></font> <font color=\"#FFFFFF\">"..msg..".</font>") end
if AUTOUPDATE then
	local ServerData = GetWebResult(UPDATE_HOST, UPDATE_PATH, "", 5)
	if ServerData then
		local ServerVersion = string.match(ServerData, "local version = \"%d+.%d+\"")
		ServerVersion = string.match(ServerVersion and ServerVersion or "", "%d+.%d+")
		if ServerVersion then
			ServerVersion = tonumber(ServerVersion)
			if tonumber(version) < ServerVersion then
				AutoupdaterMsg("New version available"..ServerVersion)
				AutoupdaterMsg("Updating, please don't press F9")
				DownloadFile(UPDATE_URL, UPDATE_FILE_PATH, function () AutoupdaterMsg("Successfully updated. ("..version.." => "..ServerVersion.."), press F9 twice to load the updated version.") end)	 
			else
				AutoupdaterMsg("You have got the latest version ("..ServerVersion..")")
			end
		end
	else
		AutoupdaterMsg("Error downloading version info")
	end
end
-- | Auto Script Update Part | Ends

-- | On Load function by zikkah ~Thanks | Starts
function OnLoad()
	require "Prodiction" -- Thanks to Klokje
	require "Collision" -- Thanks to Klokje
	Variables()
	Menu()
    Spells()
	-- Create Table structure
	for i=1, heroManager.iCount do
		local Hero = heroManager:GetHero(i)
		if Hero.team ~= myHero.team then
		EnemysInTable = EnemysInTable + 1
		EnemyTable[EnemysInTable] = { hero = Hero, Name = Hero.charName, q = 0, e = 0, r = 0, IndicatorText = "", IndicatorPos, NotReady = false, Pct = 0, PeelMe = false }
		
		end
	end
	LoadJungle()
end
-- | On Load Function | Ends

function Variables()
	Hero_Passive = {Name = "ashecritchance", Stack = 0,}
	Skills_Q = {Ready = false , Active = false, Speed = 0, Range = 0, Delay = 0, Width = 0, Name = "FrostShot"}
	Skills_W = {Ready = false , Active = false, Speed = 900, Range = 1200, Delay = 0.5, Width = 50, Name = "Volley" }
	Skills_E = {Ready = false , Active = false, Speed = 0, Range = 0, Delay = 0, Width = 0, Name = "AsheSpiritOfTheHawk" }
	Skills_R = {Ready = false , Active = false, Speed = 1600, Range = 50000, Delay = 0.5, Width = 130, Name = "EnchantedCrystalArrow" }
end	

function Skills()
	Prodiction_Q = ProdictManager.GetInstance():AddProdictionObject(_Q, Skills_Q.Range, 1100, 0.250, 200, myHero)
	Prodiction_W = ProdictManager.GetInstance():AddProdictionObject(_Q, 950, 1100, 0.250, 200, myHero)
	Prodiction_E = ProdictManager.GetInstance():AddProdictionObject(_E, 650, 1000, 0.250, 100, myHero)
	Prodiction_R = ProdictManager.GetInstance():AddProdictionObject(_R, 800, 1300, 0.250, 100, myHero)
end	
